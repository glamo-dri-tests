/*
 * gdri-buf-cmdq.c
 *
 * Test for Glamo referencing buffer objects from the command queue
 *
 * (c) 2009 Thomas White <taw@bitwiz.org.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>

#include <X11/Xmd.h>
#include <stdint.h>
#include <libdrm/drm.h>
#include <libdrm/glamo_drm.h>
#include <libdrm/glamo_bo.h>
#include <glamo_bo_gem.h>

#include "dri2.h"
#include "glamo-regs.h"
#include "utils.h"


int main()
{
	Display *dpy;
	drm_glamo_cmd_buffer_t cmdbuf;
	int r;
	int fd;
	struct glamo_bo_manager *bufmgr;
	struct glamo_bo *bo;

	RING_LOCALS;
	uint16_t *cmds;
	uint32_t *objs;
	int *obj_pos;

	printf("NOTE: This test is no longer useful.\n");

	/* Open display */
	dpy = XOpenDisplay(NULL);
	if ( dpy == NULL ) {
		fprintf(stderr, "Can't open display '%s'\n",
			XDisplayName(NULL));
	    	return 1;
	}

	fd = do_drm_authentication(dpy);
	if ( fd < 0 ) {
		fprintf(stderr, "Couldn't authenticate\n");
		return 1;
	}

	bufmgr = glamo_bo_manager_gem_ctor(fd);
	printf("Allocated a new glamo_bo_manager: 0x%p\n", bufmgr);

	bo = glamo_bo_open(bufmgr, 0, 480*480*2, 2, GLAMO_GEM_DOMAIN_VRAM, 0);

	printf("Allocated a 480x480x2 byte buffer object: 0x%p\n", bo);
	printf(" handle = %i\n", bo->handle);

	cmds = malloc(1024);
	objs = malloc(1024);
	obj_pos = malloc(1024);

	/* Render something to this buffer */
	OUT_REG_BO(GLAMO_REG_2D_DST_ADDRL, bo->handle);
	OUT_REG(GLAMO_REG_2D_DST_PITCH, (480*2) & 0x7ff);
	OUT_REG(GLAMO_REG_2D_DST_HEIGHT, 480);
	OUT_REG(GLAMO_REG_2D_PAT_FG, 0xDCBA);
	OUT_REG(GLAMO_REG_2D_COMMAND2, 0xf000);
	OUT_REG(GLAMO_REG_2D_ID1, 0);
	OUT_REG(GLAMO_REG_2D_ID2, 0);
	OUT_REG(GLAMO_REG_2D_DST_X, 0);
	OUT_REG(GLAMO_REG_2D_DST_Y, 0);
	OUT_REG(GLAMO_REG_2D_RECT_WIDTH, 480);
	OUT_REG(GLAMO_REG_2D_RECT_HEIGHT, 480);
	OUT_REG(GLAMO_REG_2D_COMMAND3, 0);

	OUT_REG(GLAMO_REG_2D_PAT_FG, 0xABAB);
	OUT_REG(GLAMO_REG_2D_DST_X, 5);
	OUT_REG(GLAMO_REG_2D_DST_Y, 5);
	OUT_REG(GLAMO_REG_2D_RECT_WIDTH, 470);
	OUT_REG(GLAMO_REG_2D_RECT_HEIGHT, 470);
	OUT_REG(GLAMO_REG_2D_COMMAND3, 0);

	cmdbuf.buf = (char *)cmds;
	cmdbuf.bufsz = 2*__count;
	cmdbuf.objs = objs;
	cmdbuf.obj_pos = obj_pos;
	cmdbuf.nobjs = __nobjs;
	r = drmCommandWrite(fd, DRM_GLAMO_CMDBUF, &cmdbuf, sizeof(cmdbuf));
	if ( r != 0 ) {
            fprintf(stderr, "DRM_GLAMO_CMDBUF failed");
            return 1;
        }
	printf("Rendered something to the buffer\n");

	/* Blit the buffer to the screen */
	__nobjs = 0;	__count = 0;
	OUT_REG_BO(GLAMO_REG_2D_SRC_ADDRL, bo->handle);
	OUT_REG(GLAMO_REG_2D_SRC_PITCH, (480*2) & 0x7ff);
	OUT_REG(GLAMO_REG_2D_DST_ADDRL, 0x0000);
	OUT_REG(GLAMO_REG_2D_DST_ADDRH, 0x0000);
	OUT_REG(GLAMO_REG_2D_DST_PITCH, (480*2) & 0x7ff);  /* The screen */
	OUT_REG(GLAMO_REG_2D_DST_HEIGHT, 640);
	OUT_REG(GLAMO_REG_2D_COMMAND2, 0xcc00);
	OUT_REG(GLAMO_REG_2D_ID1, 0);
	OUT_REG(GLAMO_REG_2D_ID2, 0);
	OUT_REG(GLAMO_REG_2D_DST_X, 100);
	OUT_REG(GLAMO_REG_2D_DST_Y, 100);
	OUT_REG(GLAMO_REG_2D_RECT_WIDTH, 480);
	OUT_REG(GLAMO_REG_2D_RECT_HEIGHT, 480);
	OUT_REG(GLAMO_REG_2D_COMMAND3, 0);
	cmdbuf.buf = (char *)cmds;
	cmdbuf.bufsz = 2*__count;
	cmdbuf.objs = objs;
	cmdbuf.obj_pos = obj_pos;
	cmdbuf.nobjs = __nobjs;
	r = drmCommandWrite(fd, DRM_GLAMO_CMDBUF, &cmdbuf, sizeof(cmdbuf));
	if ( r != 0 ) {
            fprintf(stderr, "DRM_GLAMO_CMDBUF failed");
            return 1;
        }
	printf("Copied the buffer to the screen\n");

	glamo_bo_unref(bo);

	glamo_bo_manager_gem_dtor(bufmgr);
	printf("Destroyed the glamo_bo_manager.\n");

	close(fd);
	XCloseDisplay(dpy);

	return 0;
}
