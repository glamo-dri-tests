CFLAGS=-g -W -Wall
LIBS=-lX11 -lXext -ldrm -ldrm_glamo
CC=gcc

default: gdri-cmdq-submission gdri-mem-manager gdri-buf-cmdq gdrm-gem-mmap \
         gdrm-kms-addfb gdrm-burst-cmdq gdrm-waitq

.c.o:
	$(CC) -c ${CFLAGS} ${CROSS_CFLAGS} $*.c

gdri-cmdq-submission: gdri-cmdq-submission.o dri2.o utils.o
	$(CC) gdri-cmdq-submission.o dri2.o utils.o -o gdri-cmdq-submission ${LIBS}

gdri-mem-manager: gdri-mem-manager.o dri2.o utils.o
	$(CC) gdri-mem-manager.o dri2.o utils.o -o gdri-mem-manager ${LIBS}

gdri-buf-cmdq: gdri-buf-cmdq.o dri2.o utils.o
	$(CC) gdri-buf-cmdq.o dri2.o utils.o -o gdri-buf-cmdq ${LIBS}

gdrm-gem-mmap: gdrm-gem-mmap.o drmtest.o
	$(CC) gdrm-gem-mmap.o drmtest.o -o gdrm-gem-mmap ${LIBS}

gdrm-kms-addfb: gdrm-kms-addfb.o drmtest.o
	$(CC) gdrm-kms-addfb.o drmtest.o -o gdrm-kms-addfb ${LIBS}

gdrm-burst-cmdq: gdrm-burst-cmdq.o drmtest.o
	$(CC) gdrm-burst-cmdq.o drmtest.o -o gdrm-burst-cmdq ${LIBS}

gdrm-waitq: gdrm-waitq.o drmtest.o
	$(CC) gdrm-waitq.o drmtest.o -o gdrm-waitq ${LIBS}

clean:
	rm -rf dri2.o gdri-cmdq-submission.o utils.o gdri-cmdq-submission \
		gdri-mem-manager gdri-buf-cmdq gdrm-gem-mmap gdrm-burst-cmdq \
		gdrm-waitq

install:
	install -d -m 755 ${PREFIX}/bin
	install -m 755 gdri-cmdq-submission ${PREFIX}/bin
	install -m 755 gdri-mem-manager ${PREFIX}/bin
	install -m 755 gdri-buf-cmdq ${PREFIX}/bin
	install -m 755 gdrm-gem-mmap ${PREFIX}/bin
	install -m 755 gdrm-kms-addfb ${PREFIX}/bin
	install -m 755 gdrm-burst-cmdq ${PREFIX}/bin
	install -m 755 gdrm-waitq ${PREFIX}/bin

.PHONY: clean install default
