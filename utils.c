/*
 * utils.c
 *
 * Common bits
 *
 * (c) 2009 Thomas White <taw@bitwiz.org.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <X11/Xmd.h>
#include <stdint.h>
#include <libdrm/drm.h>

#include "dri2.h"


#define RING_LOCALS int __count = 0;
#define OUT_REG(reg, val) cmds[__count++] = (reg); cmds[__count++] = (val);


int do_drm_authentication(Display *dpy)
{
	int scrnum;
	Window root;
	char *driver;
	char *device;
	int fd;
	drm_magic_t magic;

	/* Get default screen, root window and default visual */
	scrnum = DefaultScreen(dpy);
	root = RootWindow(dpy, scrnum);

	/* Get device name */
	if ( !DRI2Connect(dpy, root, &driver, &device) ) {
		fprintf(stderr, "DRI2Connect failed\n");
	    	return -1;
	}

	/* Open DRM device */
	fd = open(device, O_RDWR);
	if ( fd < 0 ) {
		fprintf(stderr, "Couldn't open '%s': %s\n",
			device, strerror(errno));
	    	return -1;
	}

	/* Get an authentication token */
	if ( drmGetMagic(fd, &magic) ) {
		fprintf(stderr, "drmGetMagic failed\n");
	    	return -1;
	}

	/* Authenticate */
	if ( DRI2Authenticate(dpy, root, magic) == False ) {
		fprintf(stderr, "DRI2Authenticate failed\n");
	    	return -1;
	}

	return fd;
}
