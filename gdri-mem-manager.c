/*
 * gdri-cmdq-submission.c
 *
 * Test for Glamo DRI's memory management
 *
 * (c) 2009 Thomas White <taw@bitwiz.org.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>

#include <X11/Xmd.h>
#include <stdint.h>
#include <libdrm/drm.h>
#include <libdrm/glamo_drm.h>
#include <libdrm/glamo_bo.h>
#include <glamo_bo_gem.h>

#include "dri2.h"
#include "glamo-regs.h"
#include "utils.h"


int main()
{
	Display *dpy;
	drm_glamo_cmd_buffer_t cmdbuf;
	uint16_t *cmds;
	int r;
	uint32_t offset = 0x000000;
	RING_LOCALS;
	int fd;
	struct glamo_bo_manager *bufmgr;
	struct glamo_bo *bo;
	struct glamo_bo *bo2;
	struct glamo_bo *bo3;
	char tmp[3];

	/* Open display */
	dpy = XOpenDisplay(NULL);
	if ( dpy == NULL ) {
		fprintf(stderr, "Can't open display '%s'\n",
			XDisplayName(NULL));
	    	return 1;
	}

	fd = do_drm_authentication(dpy);
	if ( fd < 0 ) {
		fprintf(stderr, "Couldn't authenticate\n");
		return 1;
	}

	bufmgr = glamo_bo_manager_gem_ctor(fd);
	printf("Allocated a new glamo_bo_manager: 0x%p\n", bufmgr);

	bo = glamo_bo_open(bufmgr, 0, 1024, 2, GLAMO_GEM_DOMAIN_VRAM, 0);

	printf("Allocated a 1024 byte buffer object: 0x%p\n", bo);
	printf(" handle = %i\n", bo->handle);

	bo2 = glamo_bo_open(bufmgr, 0, 16384, 2, GLAMO_GEM_DOMAIN_VRAM, 0);

	printf("Allocated a 16384 byte buffer object: 0x%p\n", bo2);
	printf(" handle = %i\n", bo2->handle);

	bo3 = glamo_bo_open(bufmgr, 0, 1024*1024, 2, GLAMO_GEM_DOMAIN_VRAM, 0);

	printf("Allocated a 1024 kilobyte buffer object: 0x%p\n", bo3);
	printf(" handle = %i\n", bo3->handle);

	printf("Press enter to continue...\n");
	fgets(tmp, 3, stdin);

	glamo_bo_unref(bo);
	printf("Unreferenced the first buffer object.\n");

	printf("I *didn't* unreference the second buffer object.\n");

	glamo_bo_unref(bo3);
	printf("Unreferenced the third buffer object.\n");

	glamo_bo_manager_gem_dtor(bufmgr);
	printf("Destroyed the glamo_bo_manager.\n");

	close(fd);
	XCloseDisplay(dpy);

	return 0;
}
