/*
 * gdrm-gem-mmap.c
 *
 * Test Glamo DRM GEM memory mapping
 *
 * (c) 2009 Thomas White <taw@bitwiz.org.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Copyright © 2008 Intel Corporation
 *
 *
 * Based on tests/gem_mmap.c from libdrm, to which the following notice applies:
 *
 * Copyright © 2008 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * Authors:
 *    Eric Anholt <eric@anholt.net>
 *
 */

#define _FILE_OFFSET_BITS 64

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <inttypes.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "drm.h"
#include "glamo_drm.h"
#include "drmtest.h"

#define OBJECT_SIZE 16384

int main(int argc, char **argv)
{
	int fd;
	struct drm_glamo_gem_create create;
	struct drm_glamo_gem_mmap mmap_arg;
	struct drm_gem_close unref;
	uint8_t expected[OBJECT_SIZE];
	uint8_t buf[OBJECT_SIZE];
	uint8_t *addr;
	uint16_t *addr16;
	int ret;
	int handle;
	char tmp[4];
	int i;

	fd = drm_open_any();

	memset(&mmap_arg, 0, sizeof(mmap_arg));
	mmap_arg.handle = 0x10101010;
	printf("Testing mmaping of bad object.\n");
	ret = ioctl(fd, DRM_IOCTL_GLAMO_GEM_MMAP, &mmap_arg);
	assert(ret == -1 && errno == EBADF);

	memset(&create, 0, sizeof(create));
	create.size = OBJECT_SIZE;
	ret = ioctl(fd, DRM_IOCTL_GLAMO_GEM_CREATE, &create);
	assert(ret == 0);
	handle = create.handle;

	printf("Testing mmaping of newly created object.\n");
	mmap_arg.handle = handle;
	mmap_arg.offset = 0;
	ret = ioctl(fd, DRM_IOCTL_GLAMO_GEM_MMAP, &mmap_arg);
	assert(ret == 0);

	printf("The offset is %8llx\n", (long long int)mmap_arg.offset);
	addr = mmap(0, OBJECT_SIZE, PROT_READ | PROT_WRITE,
		    MAP_SHARED, fd, mmap_arg.offset);
	printf("Virtual address is 0x%p\n", addr);

	printf("Press enter to continue...\n");
	fgets(tmp, 3, stdin);

	printf("Testing contents of newly created object.\n");
	addr16 = (uint16_t *)addr;
	for ( i=0; i<OBJECT_SIZE/2; i++ ) {
		addr16[i] = 0xABCD;
	}
	for ( i=0; i<OBJECT_SIZE/2; i++ ) {
		if ( addr16[i] != 0xABCD ) {
			printf("%i = %16x\n", i, addr16[i]);
		}
	}
	for ( i=0; i<OBJECT_SIZE/2; i++ ) {
		if ( addr16[i] != 0xABCD ) {
			printf("%i = %16x\n", i, addr16[i]);
		}
	}

	printf("Testing that mapping stays after close\n");
	unref.handle = handle;
	ret = ioctl(fd, DRM_IOCTL_GEM_CLOSE, &unref);
	assert(ret == 0);
	addr16 = (uint16_t *)addr;
	for ( i=0; i<OBJECT_SIZE/2; i++ ) {
		if ( addr16[i] != 0xABCD ) {
			printf("%i = %16x\n", i, addr16[i]);
		}
	}
	for ( i=0; i<OBJECT_SIZE/2; i++ ) {
		if ( addr16[i] != 0xABCD ) {
			printf("%i = %16x\n", i, addr16[i]);
		}
	}

	printf("Testing unmapping\n");
	munmap(addr, OBJECT_SIZE);

	close(fd);

	return 0;
}
