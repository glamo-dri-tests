/*
 * gdri-cmdq-submission.c
 *
 * Test for command submission via Glamo DRI
 *
 * (c) 2009 Thomas White <taw@bitwiz.org.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>

#include <X11/Xmd.h>
#include <stdint.h>
#include <libdrm/drm.h>
#include <libdrm/glamo_drm.h>

#include "dri2.h"
#include "glamo-regs.h"
#include "utils.h"


int main()
{
	Display *dpy;
	drm_glamo_cmd_buffer_t cmdbuf;
	uint16_t *cmds;
	int r;
	uint32_t offset = 0x000000;
	RING_LOCALS;
	int fd;

	printf("NOTE: This test is no longer useful.\n");

	/* Open display */
	dpy = XOpenDisplay(NULL);
	if ( dpy == NULL ) {
		fprintf(stderr, "Can't open display '%s'\n",
			XDisplayName(NULL));
	    	return 1;
	}

	fd = do_drm_authentication(dpy);
	if ( fd < 0 ) {
		fprintf(stderr, "Couldn't authenticate\n");
		return 1;
	}

	cmds = malloc(1024);

	OUT_REG(GLAMO_REG_2D_DST_ADDRL, offset & 0xffff);
	OUT_REG(GLAMO_REG_2D_DST_ADDRH, (offset >> 16) & 0x7f);
	OUT_REG(GLAMO_REG_2D_DST_PITCH, (480*2) & 0x7ff);
	OUT_REG(GLAMO_REG_2D_DST_HEIGHT, 640);
	OUT_REG(GLAMO_REG_2D_PAT_FG, 0xABCD);
	OUT_REG(GLAMO_REG_2D_COMMAND2, 0xf000);
	OUT_REG(GLAMO_REG_2D_ID1, 0);
	OUT_REG(GLAMO_REG_2D_ID2, 0);
	OUT_REG(GLAMO_REG_2D_DST_X, 100);
	OUT_REG(GLAMO_REG_2D_DST_Y, 100);
	OUT_REG(GLAMO_REG_2D_RECT_WIDTH, 280);
	OUT_REG(GLAMO_REG_2D_RECT_HEIGHT, 440);

	OUT_REG(GLAMO_REG_2D_COMMAND3, 0);

	cmdbuf.buf = (char *)cmds;
	cmdbuf.bufsz = 2*__count;
	cmdbuf.nobjs = 0;
	r = drmCommandWrite(fd, DRM_GLAMO_CMDBUF, &cmdbuf, sizeof(cmdbuf));
	if ( r != 0 ) {
            fprintf(stderr, "DRM_GLAMO_CMDBUF failed");
            return 1;
        }

	printf("Success!\n");

	close(fd);
	XCloseDisplay(dpy);

	return 0;
}
