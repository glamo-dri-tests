/*
 * utils.h
 *
 * Common bits
 *
 * (c) 2009 Thomas White <taw@bitwiz.org.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef UTILS_H
#define UTILS_H


#include <X11/Xmd.h>

#define RING_LOCALS int __count = 0;	\
		    int __nobjs = 0;

#define OUT_REG(reg, val) cmds[__count++] = (reg);	\
			  cmds[__count++] = (val);

#define OUT_REG_BO(reg, bo) objs[__nobjs] = bo;			\
			    obj_pos[__nobjs++] = __count;	\
			    cmds[__count++] = (reg);		\
			    cmds[__count++] = 0x0000;		\
			    cmds[__count++] = (reg+2);		\
			    cmds[__count++] = 0x0000;

extern int do_drm_authentication(Display *dpy);


#endif	/* UTILS_H */
