/*
 * gdrm-kms-addfb.c
 *
 * Test Glamo DRM KMS
 *
 * (c) 2009 Thomas White <taw@bitwiz.org.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Copyright © 2008 Intel Corporation
 *
 *
 * Based on tests/gem_mmap.c from libdrm, to which the following notice applies:
 *
 * Copyright © 2008 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * Authors:
 *    Eric Anholt <eric@anholt.net>
 *
 */

#define _FILE_OFFSET_BITS 64

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <inttypes.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <xf86drmMode.h>

#include "drm.h"
#include "glamo_drm.h"
#include "drmtest.h"

#define OBJECT_SIZE (640*480*2)


int main(int argc, char **argv)
{
	int fd;
	struct drm_glamo_gem_create create;
	struct drm_glamo_gem_mmap mmap_arg;
	struct drm_gem_close unref;
	uint8_t expected[OBJECT_SIZE];
	uint8_t buf[OBJECT_SIZE];
	uint8_t *addr;
	uint16_t *addr16;
	int ret;
	int handle;
	char tmp[4];
	int i;
	drmModeResPtr r;
	drmModeCrtcPtr crtc;
	int crtc_id;
	int conn_id;
	int fb_id;
	uint32_t old_buffer_id;

	fd = drm_open_any();

	printf("Creating a GEM object\n");
	memset(&create, 0, sizeof(create));
	create.size = OBJECT_SIZE;
	ret = ioctl(fd, DRM_IOCTL_GLAMO_GEM_CREATE, &create);
	assert(ret == 0);
	handle = create.handle;

	printf("Getting an mmap() offset for the new object\n");
	mmap_arg.handle = handle;
	mmap_arg.offset = 0;
	ret = ioctl(fd, DRM_IOCTL_GLAMO_GEM_MMAP, &mmap_arg);
	assert(ret == 0);

	printf("The offset is %8llx\n", (long long int)mmap_arg.offset);
	addr = mmap(0, OBJECT_SIZE, PROT_READ | PROT_WRITE,
		    MAP_SHARED, fd, mmap_arg.offset);
	addr16 = (uint16_t *)addr;
	printf("Virtual address is 0x%p\n", addr);

	r = drmModeGetResources(fd);
	if ( r == NULL ) {
		printf("Could not get DRM resources\n");
		return 1;
	}
	printf("There are %i CRTCs, %i connectors, %i encoders"
	       " and %i framebuffers\n",
	       r->count_crtcs, r->count_connectors, r->count_encoders,
	       r->count_fbs);

	crtc_id = r->crtcs[0];
	conn_id = r->connectors[0];
	printf("The first CRTC ID is %i\n", crtc_id);
	printf("The first connector ID is %i\n", conn_id);

	drmModeAddFB(fd, 480, 640, 16, 16, 480*2, handle, &fb_id);
	printf("My new FB handle is %i\n", fb_id);
	if (!fb_id) {
		printf("Could not add FB\n");
		return 1;
	}

	printf("Getting CRTC info\n");
	crtc = drmModeGetCrtc(fd, crtc_id);
	old_buffer_id = crtc->buffer_id;
	printf("Old FB handle is %i\n", old_buffer_id);

	printf("crtc=%p\n", crtc);
	if ( drmModeSetCrtc(fd, crtc_id, fb_id, 0, 0, r->connectors, 1,
	                    &(crtc->mode)) ) {
		printf("drmModeSetCrtc returned %i\n", ret);
	}

	do {

		uint16_t val;
		uint8_t expected[OBJECT_SIZE];

		/* Write to object */
		val = random() & 0xff;
		memset(addr, val, OBJECT_SIZE);

		/* Write to test buffer */
		memset(expected, val, OBJECT_SIZE);

		/* Test */
		assert(memcmp(addr, expected, OBJECT_SIZE) == 0);

		printf("Press <enter> to continue, q<enter> to finish...");
		fgets(tmp, 3, stdin);

	} while ( tmp[0] != 'q' );

	printf("Unmapping\n");
	munmap(addr, OBJECT_SIZE);

	printf("Removing FB\n");
	drmModeRmFB(fd, fb_id);

	printf("Restoring original FB\n");
	if ( drmModeSetCrtc(fd, crtc_id, old_buffer_id, 0, 0, r->connectors, 1,
	                     &(crtc->mode)) ) {
		printf("drmModeSetCrtc returned %i\n", ret);
	}

	close(fd);

	return 0;
}
